const bibliotecaController = {}

//require a nuestra conexion
const bibliotecaConnection = require('../database');

//Obtener todos los elementos
bibliotecaController.getLibros = (req, res) => {
    bibliotecaConnection.query('SELECT * FROM biblioteca', (err, rows, fields) => {
        if(!err){
            res.json(rows);
        }else {
            console.log(err);
        }
    })
}

//Obtener un elemento por titulo
bibliotecaController.getLibro = (req, res) => {
    const {titulo} = req.params;
    bibliotecaConnection.query('SELECT * FROM biblioteca WHERE titulo = ?', [titulo], (err, rows, fields) => {
        if(!err){
            if (rows != 0){
                res.json(rows)
            }else{
                res.sendStatus(404);
            }
        }else {
            console.log(err);
        }
    })
}

//Crear un nuevo elemento
bibliotecaController.createLibro = (req, res) => {

    const { titulo, autor, anio_publicacion, editorial, categoria, sede } = req.body;

    bibliotecaConnection.query('SELECT * FROM biblioteca WHERE titulo = ?', [titulo], (err, rows, fields) => {
        if(!err){
            if (rows != 0){
                res.status(405).send('Titulo de libro ya existente');
            }else{
                
                bibliotecaConnection.query('INSERT INTO biblioteca (titulo, autor, anio_publicacion, editorial, categoria, sede) VALUES (?, ?, ?, ?, ?, ?)', [titulo, autor, anio_publicacion, editorial, categoria, sede], (err, rows, fields) => {
                    if(!err){
                        bibliotecaConnection.query('SELECT * FROM biblioteca', (err, rows, fields) => {
                            if(!err){
                                res.json(rows);
                            }else {
                                console.log(err);
                            }
                        })
                    }else {
                        console.log(err);
                    }
                })

            }
        }else {
            console.log(err);
        }
    })
    
}

//Actualizar elemento
bibliotecaController.updateLibro = (req, res) => {

    const { title } = req.params;
    const { titulo, autor, anio_publicacion, editorial, categoria, sede } = req.body;

    bibliotecaConnection.query('SELECT * FROM biblioteca WHERE titulo = ?', [title], (err, rows, fields) => {
        if(!err){
            if (rows != 0){
                bibliotecaConnection.query('UPDATE biblioteca SET titulo = ?, autor = ?, anio_publicacion = ?, editorial = ?, categoria = ?, sede = ? WHERE `titulo` = ?', [titulo, autor, anio_publicacion, editorial, categoria, sede, title], (err, rows, fields) => {
                    if(!err){
                        res.sendStatus(200);
                    }else {
                        console.log(err);
                    }
                })
            }else{
                res.sendStatus(404);
            }
        }else {
            console.log(err);
        }
    })
    
}

//Borrar elemento
bibliotecaController.deleteLibro = (req, res) => {

    const {listName} = req.params;

    bibliotecaConnection.query('SELECT * FROM biblioteca WHERE titulo = ?', [listName], (err, rows, fields) => {
        if(!err){
            if (rows != 0){
                bibliotecaConnection.query('DELETE FROM biblioteca WHERE titulo = ?', [listName], (err, rows, fields) => {
                    if(!err){
                        res.sendStatus(200);
                    }else {
                        console.log(err);
                    }
                })
            }else{
                res.sendStatus(404);
            }
        }else {
            console.log(err);
        }
    })

    

}

module.exports = bibliotecaController;