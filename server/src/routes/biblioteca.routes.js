const { Router } = require('express');
const router = Router();

const bibliotecaController = require('../cnotrollers/biblioteca.controller.js');

//Creacion de nuestras rutas
//GET ALL
router.get('/', bibliotecaController.getLibros);

//GET BY TITULO
router.get('/:titulo', bibliotecaController.getLibro);

//CREATE
router.post('/', bibliotecaController.createLibro);

//UPDATE
router.put('/:title', bibliotecaController.updateLibro);

//DELETE
router.delete('/:listName', bibliotecaController.deleteLibro);

module.exports = router;