const mysql = require('mysql');

//Conexion a la BD
const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'cidesi',
    insecureAuth : true
});

//Verificar Conexion
mysqlConnection.connect(function (err){
    if(err){
        console.log(err);
        return;
    }else {
        console.log('DB CONNECTED')
    }
});

module.exports = mysqlConnection;